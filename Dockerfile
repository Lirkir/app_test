FROM python:2

COPY src /app

ENTRYPOINT ["python", "/app/app.py"]
